__author__ = 'ror191505'
from peewee import *


db = SqliteDatabase('intel.db')

class Chat(Model):
    id = IntegerField(index=True, unique=True, primary_key=True)
    intel = BooleanField(default=False)

    class Meta:
        database = db

class User(Model):
    id = IntegerField(index=True, unique=True, primary_key=True)
    admin = BooleanField(default=False)

    class Meta:
        database = db

