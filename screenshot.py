
from PySide import QtWebKit

from ghost.ghost import Ghost, Error
from cookielib import CookieJar



def authorize(cookie_jar):
    ghost = Ghost(display=True)
    ghost.open("https://www.ingress.com/intel")
    ghost.click('div.button:nth-child(2) > a:nth-child(1)', expect_loading=True)
    try:
        ghost.fill("#gaia_loginform", {"Email": "ubedelan", "Passwd": "ubedelan", })
        print 'one'
    except Error:
        print 'two'
        ghost.fill("#gaia_loginform", {"Email": "ubedelan"})
        ghost.fire("#next", 'click')
        ghost.sleep(1)
        ghost.fill("#gaia_loginform", {"Passwd": "ubedelan"})
    ghost.fire('#signIn', 'click', expect_loading=True)
    ghost.save_cookies(cookie_jar)
    ghost.exit()


def get_cookies():
    cj = CookieJar()
    authorize(cj)
    return cj


def get_screenshot(url, path):
    print 'Get Image'
    cookie_jar = get_cookies()

    ghost = Ghost(display=True)
    ghost.load_cookies(cookie_jar)

    ghost.open(url)

    ghost.set_viewport_size(1200, 750)
    print 'Get Image (wait)'

    ghost.sleep(1)
    ghost.wait_for(lambda: ghost.main_frame.findFirstElement('#loading_msg').styleProperty('display', QtWebKit.QWebElement.ComputedStyle)=='none', '', 100)
    ghost.evaluate('''
            document.getElementById("player_stats").remove();
            document.getElementById("header").remove();
            document.getElementById("geotools").remove();
            document.getElementById("game_stats").remove();
            document.getElementById("comm").remove();
            document.getElementById("filters_container").remove();
            document.getElementById("portal_filter_header").remove();
            document.getElementById("tm_button").remove();
            document.getElementById("clear_planned_links").remove();
            document.getElementById("display_msg").remove();
            document.getElementById("loading_msg").remove();
            document.getElementById("butterbar").remove();
            document.getElementById("zoom_level_data").remove();
            document.getElementById("dashboard_container").style.top = "0px";
            document.getElementById("dashboard_container").style.bottom = "0px";
            document.getElementById("dashboard_container").style.left = "0px";
            document.getElementById("dashboard_container").style.right = "0px";
            document.getElementById("dashboard_container").style.border = "0px";
            document.querySelector('.gm-style > div:nth-child(10)').remove();
            document.querySelector('.gm-style > div:nth-child(9)').remove();
            document.querySelector('.gm-style > div:nth-child(8)').remove();
            document.querySelector('.gm-style > div:nth-child(7)').remove();
            document.querySelector('.gm-style > div:nth-child(6)').remove();
            document.querySelector('.gm-style > div:nth-child(5)').remove();
            document.querySelector('.gm-style > div:nth-child(4)').remove();
            document.querySelector('.gm-style > div:nth-child(3)').remove();
            document.querySelector('.gm-style > div:nth-child(2)').remove();
''')
    ghost.fire('#dashboard', 'resize')
    ghost.sleep(1)

    ghost.evaluate('''
            document.getElementById('tab_content_mod').style.display='block';
            document.getElementById('resonators_health').remove();
            document.getElementById('resonatorsCenterPiece').remove();
            document.querySelector('#portal_tabs_container > div:nth-child(1)').remove();
            document.getElementById('lp_button').remove();

            var icons = document.getElementsByClassName('mod_icon');
            var icons_len = icons.length;
            for (var i = 0; i < icons_len; i++) { icons[0].remove(); }

            var separator = document.getElementsByClassName('mod_separator');
            var separator_len = separator.length;
            for (var i = 0; i < separator_len; i++) { separator[0].remove(); }
    ''')

    ghost.capture_to(path, selector='#map_canvas')
    ghost.exit()
