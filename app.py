from db import Chat, User
from message import IntelMessage, send_photo
from tornado.web import Application, RequestHandler
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from tornado.escape import json_decode
from tempfile import NamedTemporaryFile
from screenshot import get_screenshot

BOT_ID = 112339751


class App(Application):
    def __init__(self, settings={}):
        handlers = [
            (r'/intel_screenshot/', MainHandler)
        ]
        Application.__init__(self, handlers, **settings)


def process_intel_url(intel_url, message):
    with NamedTemporaryFile(suffix='.png') as picture:
        get_screenshot(intel_url, picture.name)
        # TODO: fix unknown situation with empty file
        picture.file.seek(0)
        send_photo(message.chat.id, picture.file)


def process_bot_addition(message):
    user, created = User.get_or_create(id=message.user.id)
    if not created and user.admin:
        add_bot(message)


def add_bot(message):
    chat, created = Chat.get_or_create(id=message.chat.id)
    chat.intel = True
    chat.save()
    print 'new bot'


def check_chat(message):
    chat, created = Chat.get_or_create(id=message.chat.id)
    return chat.intel


def process_message(message):
    intel_url = message.get_intel_url()
    if intel_url:
        process_intel_url(intel_url, message)


class MainHandler(RequestHandler):
    def post(self, *args, **kwargs):
        data = json_decode(self.request.body)
        message = IntelMessage(data['message'])
        print message
        if getattr(message.new_chat_participant, 'id', 0) == BOT_ID:
            process_bot_addition(message)
        if check_chat(message):
            process_message(message)


if __name__ == '__main__':
    http_server = HTTPServer(App())
    http_server.listen(8889)
    IOLoop.instance().start()
