__author__ = 'ror191505'
from datetime import datetime
import re
from urlparse import urlparse

import requests

API_KEY = '112339751:AAHuJbOM2wkEZdQwcg2F3IVkotKWsZ-Cszs'


def _object_or_none(data, klass):
    if data:
        return klass(data)
    return None


class BaseMessage(object):
    def __init__(self, message_data):
        self.message_id = message_data['message_id']
        self.user = User(message_data.get('from'))
        self.date = datetime.fromtimestamp(message_data['date'])

        try:
            self.chat = User(message_data['chat'])
        except KeyError:
            self.chat = GroupChat(message_data['chat'])

        self.forward_from = _object_or_none(message_data.get('forwarded_from'), User)
        self.forward_date = _object_or_none(message_data.get('forward_date'), datetime.fromtimestamp)
        self.text = message_data.get('text', '')
        self.audio = _object_or_none(message_data.get('audio'), Audio)
        self.document = _object_or_none(message_data.get('document'), Document)
        self.photo = map(lambda data: _object_or_none(data, PhotoSize), message_data.get('photo', []))
        self.sticker = _object_or_none(message_data.get('sticker'), Sticker)
        self.video = _object_or_none(message_data.get('video'), Video)
        self.contact = _object_or_none(message_data.get('contact'), Contact)
        self.location = _object_or_none(message_data.get('location'), Location)
        self.new_chat_participant = _object_or_none(message_data.get('new_chat_participant'), User)
        self.left_chat_participant = _object_or_none(message_data.get('left_chat_participant'), User)
        self.new_chat_title = message_data.get('new_chat_title')
        self.delete_chat_photo = message_data.get('delete_chat_photo', False)
        self.group_chat_created = message_data.get('group_chat_created', False)

    def __unicode__(self):
        return u'id{}, user: {}, chat: {}'.format(self.message_id, self.user, self.chat)

    def __str__(self):
        return unicode(self).encode('utf-8')


class IntelMessage(BaseMessage):
    def get_intel_url(self, zoom=15):
        if self.location:
            return 'https://www.ingress.com/intel?ll={},{}&z={}'.format(self.location.longitude, self.location.latitude,
                                                                        zoom)
        elif self.text:
            parser = re.search('(?P<url>https?://[^\s]+)', self.text)
            url = urlparse(parser.group('url')) if parser else urlparse('')
            return url.geturl() if url.netloc.endswith('ingress.com') and url.path.startswith('/intel') else ''
        else:
            return ''


class BaseUser(object):
    def __init__(self, data):
        self.first_name = data['first_name']
        self.last_name = data.get('last_name', '')
        self.username = data.get('username', '')

    def __unicode__(self):
        return u'{} {} ({})'.format(self.first_name, self.last_name, self.username)

    def __str__(self):
        return unicode(self).encode('utf-8')


class User(BaseUser):
    def __init__(self, data):
        super(User, self).__init__(data)
        self.id = data['id']

    def __unicode__(self):
        return u'id{}'.format(self.id)

    def __str__(self):
        return unicode(self).encode('utf-8')


class GroupChat(object):
    def __init__(self, group_chat_data):
        self.id = group_chat_data['id']
        self.title = group_chat_data['title']

    def __unicode__(self):
        return u'id{} {}'.format(self.id, self.title)

    def __str__(self):
        return unicode(self).encode('utf-8')


class File(object):
    def __init__(self, file):
        self.file_id = file['file_id']
        self.file_size = file.get('file_size', '')
        self.file_name = file.get('file_name', '')

    def __unicode__(self):
        return u'{} {} {}'.format(self.file_id, self.file_size, self.file_name)

    def __str__(self):
        return unicode(self).encode('utf-8')


class DurationFile(File):
    def __init__(self, data):
        super(DurationFile, self).__init__(data)
        self.duration = data['duration']

    def __unicode__(self):
        return u'{}'.format(self.duration)

    def __str__(self):
        return unicode(self).encode('utf-8')


class ThumbFile(File):
    def __init__(self, data):
        super(ThumbFile, self).__init__(data)
        self.thumb = _object_or_none(data.get('thumb'), PhotoSize)


class ImageFile(File):
    def __init__(self, data):
        super(ImageFile, self).__init__(data)
        self.width = data['width']
        self.height = data['height']

    def __unicode__(self):
        return u'{}x{}'.format(self.width, self.height)

    def __str__(self):
        return unicode(self).encode('utf-8')


class PhotoSize(ImageFile):
    def __init__(self, data):
        super(PhotoSize, self).__init__(data)


class Audio(File):
    def __init__(self, data):
        super(Audio, self).__init__(data)
        self.duration = data['duration']

    def __unicode__(self):
        return u'duration: {}'.format(self.duration)

    def __str__(self):
        return unicode(self).encode('utf-8')


class MimeFile(File):
    def __init__(self, data):
        super(MimeFile, self).__init__(data)
        self.mime_type = data['mime_type']

    def __unicode__(self):
        return u'mime: {}'.format(self.mime_type)

    def __str__(self):
        return unicode(self).encode('utf-8')


class Document(ThumbFile, MimeFile):
    def __init__(self, data):
        super(Document, self).__init__(data)


class Sticker(ImageFile, ThumbFile):
    def __init__(self, data):
        super(Sticker, self).__init__(data)


class Video(ImageFile, ThumbFile, DurationFile, MimeFile):
    def __init__(self, data):
        super(Video, self).__init__(data)
        self.caption = data.get('caption')

    def __unicode__(self):
        return u'"{}"'.format(self.caption)

    def __str__(self):
        return unicode(self).encode('utf-8')


class Contact(BaseUser):
    def __init__(self, data):
        super(Contact, self).__init__(data)
        self.user_id = data.get('user_id')

    def __unicode__(self):
        return u'id{}'.format(self.user_id)

    def __str__(self):
        return unicode(self).encode('utf-8')


class Location(object):
    def __init__(self, data):
        self.longitude = data['longitude']
        self.latitude = data['latitude']

    def __unicode__(self):
        return u'{}:{}'.format(self.longitude, self.latitude)

    def __str__(self):
        return unicode(self).encode('utf-8')


def send_photo(chat_id, photo, caption=None, reply_to_message_id=None, reply_markup=None):
    data = {'chat_id': chat_id, 'reply_to_message_id': reply_to_message_id, 'caption': caption}
    files = {'photo': ('screen.png', photo)}
    send_data('sendPhoto', data, files)


def send_data(method, data, files=None):
    # TODO: add error handling
    url = 'https://api.telegram.org/bot{}/{}'.format(API_KEY, method)
    print requests.post(url, data=data, files=files).text
